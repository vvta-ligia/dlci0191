package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    EmployeeMock em;

    @BeforeEach
    void setUp()
    {
        EmployeeImpl empl= new EmployeeImpl();
        em= new EmployeeMock();

    }

    @Test
    void addEmployee_TC1() {
        Employee e = new Employee();
        e.setId(9);
        e.setLastName("Antonescu");
        e.setFirstName("Maria");
        e.setCnp("2134567891212");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500.00);

        try {
            int nEmployee = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nEmployee + 1, em.getEmployeeList().size());
                System.out.println("TRUE");
            } catch (Exception exc)
            {
                exc.printStackTrace();
                assert(false);

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }


    @Test
    void addEmployee_TC2() {
        Employee employee = new Employee(
                "Popescu",
                "Valentina",
                "21345678912356",
                DidacticFunction.ASISTENT,
                2501.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        //assertTrue(added);
        assertFalse(added);
        System.out.println("FALSE");
    }


    @Test
    void addEmployee_TC4() {
        Employee employee = new Employee(
                "popescu",
                "Maria",
                "213456789123",
                DidacticFunction.LECTURER,
                2503.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC5() {
        Employee employee = new Employee(
                "Popescu",
                "camelia",
                "213456789123",
                DidacticFunction.TEACHER,
                2504.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC6() {
        Employee employee = new Employee(
                "@opescu",
                "Maria",
                "2134567891234",
                DidacticFunction.ASISTENT,
                2500.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }

    @Test
    void addEmployee_TC7() {
        Employee e = new Employee();
        e.setId(15);
        e.setLastName("Antonescu");
        e.setFirstName("Maria");
        e.setCnp("2134567891235");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500.00);

        try {
            int nEmployee = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nEmployee + 1, em.getEmployeeList().size());
                System.out.println("TRUE");
            } catch (Exception exc)
            {
                exc.printStackTrace();
                assert(false);

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }
    @Test
    void addEmployee_TC8() {
        Employee e = new Employee();
        e.setId(12);
        e.setLastName("Zidaru");
        e.setFirstName("Maria");
        e.setCnp("2134567891235");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500.00);

        try {
            int nEmployee = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nEmployee + 1, em.getEmployeeList().size());
                System.out.println("TRUE");
            } catch (Exception exc)
            {
                exc.printStackTrace();
                assert(false);

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }
    @Test
    void addEmployee_TC9() {
        Employee employee = new Employee(
                "]opescu",
                "Maria",
                "213456789123",
                DidacticFunction.ASISTENT,
                2500.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }
    @Test
    void addEmployee_TC10() {
        Employee employee = new Employee(
                "Popescu",
                "@aria",
                "213456789123",
                DidacticFunction.ASISTENT,
                2500.00);
        int noOfEmployees = em.getEmployeeList().size();
        boolean added = em.addEmployee(employee);
        assertEquals(noOfEmployees, em.getEmployeeList().size());
        assertFalse(added);
        System.out.println("FALSE");
    }


    @Test
    void addEmployee_TC12() {
        Employee e = new Employee();
        e.setId(8);
        e.setLastName("Popescu");
        e.setFirstName("Zamfir");
        e.setCnp("1134567891210");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500.00);

        try {
            int nEmployee = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nEmployee + 1, em.getEmployeeList().size());
                System.out.println("TRUE");
            } catch (Exception exc)
            {
                exc.printStackTrace();
                assert(false);

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }




    @Test
    void modifyEmployeeFunction_TC01() {

        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee employee = employeeList.get(0);

        System.out.println("Initial function was ASISTENT");
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());

        employeeMock.modifyEmployeeFunction(employee, DidacticFunction.LECTURER);

        System.out.println("Modified function is LECTURER");
        assertEquals(DidacticFunction.LECTURER, employee.getFunction());
    }

    @Test
    void modifyEmployeeFunction_TC02() throws EmployeeException {

        EmployeeMock employeeMock = new EmployeeMock(0);
        List<Employee> employeeList = employeeMock.getEmployeeList();

        assertEquals(0, employeeList.size());
    }

    @Test
    void modifyEmployeeFunction_TC03() {

        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        employeeMock.modifyEmployeeFunction(null, DidacticFunction.LECTURER);

        List<Employee> newEmployeeList = employeeMock.getEmployeeList();

        assertEquals(employeeList, newEmployeeList);
        System.out.println("Employee List not modified");
    }
}