package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.opentest4j.AssertionFailedError;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    Employee employee;

    @BeforeEach
    void setUpTest() {
        employee = new Employee(
                "Ligia-Paula",
                "Danci",
                "2940118244064",
                DidacticFunction.TEACHER,
                4321.1);
    }

    @Test
    void testConstructorSetsFieldsCorrectly() {
        assertEquals("Ligia-Paula", employee.getFirstName());
        assertEquals("Danci", employee.getLastName());
        assertEquals("2940118244064", employee.getCnp());
        assertEquals(DidacticFunction.TEACHER, employee.getFunction());
        assertEquals(4321.1, employee.getSalary());
    }

    @Test
    void getId() {
        assertEquals(0, employee.getId());
    }

    @Test
    void setId() {
        employee.setId(2);
        assertEquals(2, employee.getId());
    }

    @Test
    void getFirstName() {
        assertEquals("Ligia-Paula", employee.getFirstName());
    }

    @Test
    void setFirstName() {
        employee.setFirstName("Mihai");
        assertEquals("Mihai", employee.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("Danci", employee.getLastName());
    }

    @Test
    void setLastName() {
        employee.setLastName("Popescu");
        assertEquals("Popescu", employee.getLastName());
    }


    @Test
    void getCnpNullPointer() {
        Employee employee = null;
        assertThrows(NullPointerException.class,()->employee.getCnp());
        System.out.println("Employee has no CNP");
    }

    @Test
    void getFunctionNullPointer() {
        Employee employee = null;
        assertThrows(NullPointerException.class,()->employee.getFunction());
        System.out.println("Employee has no function");
    }

    @ParameterizedTest
    @NullSource
    public void testSetLastNameNull(String lastName){
        employee.setLastName(lastName);
        System.out.println("Test parameterized with NULL source");
        assertNull(employee.getLastName());
    }

    @ParameterizedTest
    @EmptySource
    public void testSetLastNameEmpty(String lastName){
        employee.setLastName(lastName);
        System.out.println("Test parameterized with EMPTY source");
        assertEquals("", employee.getLastName());
    }
    @ParameterizedTest
    @ValueSource(strings = {"Popescu", "Danci", "Coman"})
    public void testSetLastNameWithValue(String lastName){
        employee.setLastName(lastName);
        System.out.println("Test parameterized with VALUES source: " + lastName);
        assertEquals(lastName, employee.getLastName());
    }


}