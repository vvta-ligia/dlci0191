package AngajatiApp.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.opentest4j.AssertionFailedError;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SalaryCriteriaTest {
    private SalaryCriteria toTest;

    @BeforeEach
    public void  init() {
        toTest = new SalaryCriteria();
        System.out.println("<******************************************************************>");
    }

    @Test
    @Order(2)
    public void tesCompareWithSuccess() {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee();

        //  = Equal salary=
        System.out.println("Equal salary");
        assertEquals(0, toTest.compare(employee1, employee2));
        employee1.setSalary(23d);
        employee2.setSalary(23d);
        assertEquals(0, toTest.compare(employee1, employee2));

        //  < Less salary
        System.out.println("Less salary");
        employee1.setSalary(50d);
        assertEquals(-27, toTest.compare(employee1, employee2));

        //  > Greater salary
        System.out.println("Greater salary");
        employee2.setSalary(70d);
        assertEquals(20, toTest.compare(employee1, employee2));
    }


}
